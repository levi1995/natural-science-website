import * as React from "react";
import NavigationButton from "../../Router/components/NavigationButton/NavigationButton";
import RouterService from "../../Router/RouterService"
import { IAB } from "../../NewAbout/NewAboutStore";
interface Props {
  About: IAB;
}
const About = (props: Props) => (
  <NavigationButton Link={RouterService.getNavlink.AboutPage(props.About.Keys.join("-"))} Label={props.About.Title}  /> 
);

export default About