
import * as React from "react";
import { IAB } from "../../NewAbout/NewAboutStore"

interface Props {
    Abaut: IAB;
}
const AboutDissplayer = (props: Props) => {
    const { Abaut } = props;

    let getFileByName = (fileName: string): string => {
        let file = Abaut.Files.find(f => f.Name == fileName)
        if (file) return file.Data as string;
        return null;
    }

    return (<div style={{
        margin: "3rem",
        marginBottom: "1rem",
        marginTop: "2rem",
        borderWidth: "1px",
        //borderColor: "black",
        borderStyle: "groove",
        backgroundColor: "aliceblue",
        borderColor: "transparent",
        wordBreak: "break-word",
    }}>
        <div style={{
            display: "flex",
            flexDirection: "row",
            marginBottom: "1rem"
        }}>
            <div style={{ marginRight: "auto", fontSize: "xxx-large" }}>{Abaut.Title} </div>

        </div>
        <div>{Abaut.Description.map((ds, indexP) => {
            let tagRegexp = /(<-Bold->)|(<-\/Bold->)|(<-IMG->)|(<-\/IMG->)|(<-Ancore->)|(<-\/Ancore->)/g
            let boldStartRegexp = /(<-Bold->)/g;
            let boldEndRegexp = /(<-\/Bold->)/g;
            let imgStartregexp = /(<-IMG->)/g;
            let imgEndregexp = /(<-\/IMG->)/g;
            let ancoreStartregexp = /(<-Ancore->)/g;
            let ancoreEndregexp = /(<-\/Ancore->)/g;
            let strings = ("<-S->" + ds + "<-S->").split(tagRegexp).filter(s => !(s === undefined || s === null))
            let ancore = false;
            let bold = false;
            let img = false;
            let ConfiguratedStrings: Array<{ modifiers: Array<string>, string: string }> = [];
            strings.forEach((s, i) => {
                if (s.match(tagRegexp)) {
                    if (s.match(boldStartRegexp)) bold = true;
                    if (s.match(boldEndRegexp)) bold = false;
                    if (s.match(imgStartregexp)) img = true;
                    if (s.match(imgEndregexp)) img = false;
                    if (s.match(ancoreStartregexp)) ancore = true;
                    if (s.match(ancoreEndregexp)) ancore = false;
                }
                else {
                    let modifires: Array<string> = [];
                    if (ancore) modifires.push("Ancore")
                    if (bold) modifires.push("Bold")
                    if (img) modifires.push("IMG")
                    ConfiguratedStrings.push({ modifiers: modifires, string: s.replace(/<-S->/g, "") })
                }
            });
            return <p style={{ paddingLeft: "10px" }}>{ConfiguratedStrings.map((sonfs, index) => {
                let content = <samp key={Abaut.Title + "_P_" + indexP + "_" + index} style={{ fontSize: "larger", }}>{sonfs.string}</samp>;

                if (sonfs.modifiers.includes("IMG")) {
                    return <div style={{ textAlign: "center", float: "initial", }}><img style={{ maxWidth: "100%" }} src={getFileByName(sonfs.string)}></img></div>
                }
                else {
                    if (sonfs.modifiers.includes("Bold")) content = (
                        <samp key={Abaut.Title + "_P_" + indexP + "B_" + index} style={{ fontWeight: "bold" }}>
                            {content}
                        </samp>)
                    if (sonfs.modifiers.includes("Ancore")) content = (< span key={Abaut.Title + "_P_" + indexP + "A_" + index} ref="my-anchor" >{content}</span>)
                }
                return content;
            })}</p>
        })}</div>
    </div >)
};

export default AboutDissplayer


