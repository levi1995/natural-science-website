import { action, computed, makeObservable, observable } from "mobx"
import { IAB } from "../NewAbout/NewAboutStore";

export interface IAbaut {
    Id: string;
    Title: string;
    Keys: Array<string>;
    Description: string;
}

export const testA: Array<IAbaut> = [
    {
        Id: "Albert Einstein",
        Title: "Albert Einstein",
        Keys: ["Einstein"],
        Description: `Albert Einstein (Ulm, 1879. március 14. – Princeton, 1955. április 18.) zsidó származású német Nobel-díjas elméleti fizikus; egyes tudományos és laikus körökben a legnagyobb 20. századi tudósnak tartják. Ő dolgozta ki a relativitáselméletet és nagymértékben hozzájárult a kvantummechanika, a statisztikus mechanika és a kozmológia fejlődéséhez. 1921-ben fizikai Nobel-díjjal jutalmazták „az elméleti fizika területén szerzett eredményeiért, különös tekintettel a fényelektromos jelenség törvényszerűségeinek felismeréséért”. Zsenialitására jellemző, hogy előre tudott jelezni olyan dolgokat, amelyeket 100 évvel később a gyakorlatban is bizonyítottak. 2016. február 11-én amerikai kutatók bejelentették, hogy gravitációs hullámokkal kapcsolatos elmélete igazolódott, ezzel a felfedezéssel egy új tudományág született, a gravitációshullám-csillagászat.[1][2]

        A hétköznapi emberek bizonyos köreiben ő vált a legmagasabb fokú zsenialitás szinonimájává, arcképe egyike a legismertebbeknek a világon. 1999-ben a Time folyóirat az „évszázad emberének” választotta.`,
    },
    {
        Id: "lo",
        Title: "Ló",
        Keys: ["lo"],
        Description: `4 lába van`,
    },
    {
        Id: "JavaScript",
        Title: "JavaScript és Java",
        Keys: ["JavaScript", "Java"],
        Description: `A Java
        A Java nyelvet a Sun Microsystems alkotta meg. Platform független, objektum-orientált nyelv, melynek megjelenése megmutatta az utat az interaktív weboldalak felé. A JavaScript nyelvet semmi esetre se keverjük össze a Java nyelvvel. A névbeli hasonlóság nem a véletlen műve, mert a JavaScriptet a Java alapján, annak esetenkénti kiváltására fejlesztették ki. Sok a különbség, így az elnevezés inkább megtévesztő.
        
        A JavaScript és a Java
        A Java nyelv használata még túl nagy ugrás az egyszerű szöveges HTML szerkezetek után. A JavaScript tulajdonképpen egy közbülső állomás a HTML és a Java között. Ha csak egy kicsit szeretnénk élőbbé varázsolni oldalainkat, nem kell mindjárt a Java nyelvhez nyúlni, ami sok esetben körülményes. A JavaScript nyelv viszont egy sor egyszerűen elérhető szolgáltatást nyújt, amit egy hétköznapi felhasználó is könnyen beilleszt a dokumentumba.`,
    },

]

export class AboutStore {
    Abauts: Array<IAB> = [];
    Filter: string = null;
    Selecteds: Array<IAB> = null;

    constructor(Abaut: Array<IAB>) {
        makeObservable(this, {
            Selecteds: observable,
            Abauts: observable,
            Filter: observable,
            selectAbauts: action,
        })
        this.Abauts = this.help(Abaut);
    }

    selectAbauts(keys: Array<string>) {
        if (this.Filter != keys.join("-")) {
            this.Selecteds = this.Abauts.filter(abaut => this.union(keys, abaut.Keys).length > 0);
            this.Filter = keys.join("-");
        }
    }

    union(sa1: Array<string>, sa2: Array<string>): Array<string> {
        let ret: Array<string> = [];
        if (sa1 && sa2) {

            sa1.forEach(sa => {
                if (sa2.includes(sa)) {
                    ret.push(sa)
                }
            });
        }
        return ret;
    }

    help(obj: object) {
        if (!obj) return [];
        return JSON.parse(JSON.stringify(obj))
    }

}