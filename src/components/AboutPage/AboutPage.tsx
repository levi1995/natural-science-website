import { observer } from "mobx-react";
import * as React from "react";
import { useParams } from "react-router-dom";
import { AboutStore } from "./AboutStore";
import About from "./Components/About"
import AboutDissplayer from "./Components/AboutDissplayer";
interface Props {
  AboutStore: AboutStore;
  id?: string;
}
const AboutPage = observer(({ AboutStore }: Props) => {
  let { id } = useParams() as any;

  if (id) {
    AboutStore.selectAbauts((id as string).split("-"))
  } else {
    AboutStore.selectAbauts([])
  }

  if (AboutStore.Selecteds && AboutStore.Selecteds.length > 0) return (
    <React.Fragment>
      {AboutStore.Selecteds.map(selected => (
       <AboutDissplayer Abaut={selected} />
      ))}
    </React.Fragment>
  )

  return (<div style={{
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  }}>
    {AboutStore.Abauts.map(about => <About About={about} />)}
  </div>)

});

export default AboutPage