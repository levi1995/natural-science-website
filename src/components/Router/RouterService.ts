class RouterService {
    location = window.location.href;

    get getNavlink() {
        return {
            MainPage: () => "/",
            AboutPage: (about?: string) => about ? `about/${about}` : "/about",
            GamePage: () => "/game",
            AdminAbout: () => "/newAbout",
            AdminGame: () => "/game",
        }
    }

}

export default new RouterService()