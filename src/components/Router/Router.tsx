import * as React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import MainPage from "../MainPage/MainPage"
import AboutPage from "../AboutPage/AboutPage"
import GamePage from "../GamePage/GamePage"
import NewAboutPage from "../NewAbout/NewAbout"
import { Game } from "../GamePage/GameStore";
import { AboutStore } from "../AboutPage/AboutStore";
import { NewAboutStore } from "../NewAbout/NewAboutStore";
import { ABCONFIG } from "../AboutPage/Consts/AboutConfiigs"

export default function MainRouter() {
    let aboutStore = new AboutStore(ABCONFIG);
    return (
        <Router>
            <div style={{ height: "inherit" }}>
                <div className="NavBar">

                    <Link to="/"><div>Kezdőlap</div></Link>
                    <Link to="/about"><div>Olvasmányok</div></Link>
                    <Link to="/game"><div>Játék</div></Link>
                    <Link to="/newAbout"><div>Admin</div></Link>

                </div>
                <Switch>
                    <Route exact path="/">
                        <MainPage />
                    </Route>
                    <Route path="/about/:id" children={<AboutPage AboutStore={aboutStore} />} />
                    <Route path="/about">
                        <AboutPage AboutStore={aboutStore} />
                    </Route>
                    <Route path="/game">
                        <GamePage Game={new Game(ABCONFIG)} />
                    </Route>
                    <Route path="/newAbout">
                        <NewAboutPage NewAboutStore={new NewAboutStore()} />
                    </Route>
                </Switch>
            </div>
        </Router >
    );
}

