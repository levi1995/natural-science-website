import * as React from "react";
import { Link } from "react-router-dom";
interface Props {
    Label: string;
    Link: string;
}
const NavigationLink = (props: Props) => (
    <Link  to={props.Link} ><div style={{

    }}>{props.Label}</div></Link>
);

export default NavigationLink