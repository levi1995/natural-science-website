import * as React from "react";
import { Link } from "react-router-dom";

interface Props {
    Label: string;
    Link: string;
}
const NavigationButton = (props: Props) => (
    <Link className={"NavButoon"} style={{}} to={props.Link} ><div style={{

    }}>{props.Label}</div></Link>
);

export default NavigationButton