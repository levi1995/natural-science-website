import * as React from "react";
import AnimationWarper from "../AnimationWarper/AnimationWarper"
interface Props {

}
const MainPage = (props: Props) => (
  <div className={"home"}>
    <div style={{width: "fit-content"}}>
      <h1>{"Természettudományos kvíz játék"}</h1>
      <h4>{"A játékhoz a fenti játék gomb segítségével tudsz eljutni."}</h4>
      <h3>{"A játék menete"}</h3>
      <ul>
        <li>{"6 kérsést lessz minden kérdés 1 pontot ér a válaszra katintva küdheted be a megfejtésedet."}</li>
        <li>{"A játék végen láthatod a pontjaidat és egy olvasmányok linket arra katintva a kérdésekhez kapcsolódó olvasmányokat tudod megnézni."}</li>
        <li> {"Jó szórakozást."}</li>
      </ul>
    </div>
  </div>
);

export default MainPage