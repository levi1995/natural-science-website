import * as React from "react";
interface Props {
    children: any;
}
const AnimationWarper = (props: Props) => {
    let style: React.CSSProperties = {
        backgroundColor: "transparent",
        position: "absolute",
        listStyle: "none",
        display: "block",
    };

    let style3: React.CSSProperties = {
        width: "4rem",
        height: "4rem",
        borderStyle: "solid",
        borderWidth: "0.1rem",
        borderColor: "transparent",
        borderRadius: "100%",
        animationIterationCount: "infinite",
        animationDirection: "normal",
        animationName: "boobles", //"boobles",
    };

    let style4: React.CSSProperties = {
        left: "10%",
        animationDuration: "10s",
        animationDelay: "0s",
    };

    let getAnimationCongig = () => {

        let colores = ["yellow", "greenyellow", "pink", "palegoldenrod", "salmon"];
        let colore = colores[+(Math.random() * (colores.length - 1)).toFixed(0)];
        let width = ((Math.random() * 6) + 2).toFixed(0);
        let left = (Math.random() * 100).toFixed(0);
        let duration = ((Math.random() * 10) + 10).toFixed(0);
        let animationDelay = (Math.random() * 5).toFixed(0);
        return {
            top:"-150%",
            backgroundColor: "transparent",
            borderColor: colore,
            width: width + "rem",
            height: width + "rem",
            left: left + "%",
            animationDuration: duration + "s",
            animationDelay: animationDelay + "s",
        }
    }


    return (<React.Fragment>
        <ul className="AnimationWarper">
            {"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15".split(" ").map((s, index) => {
                return <li style={{ ...style, ...style3, ...getAnimationCongig() }}></li>
            })}
        </ul>
        {props.children}
    </React.Fragment>)
};

export default AnimationWarper