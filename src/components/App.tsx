import * as React from "react";
import MainRouter from "./Router/Router"
import './App.css';
export interface HelloWorldProps {
  userName: string;
  lang: string;
}
export const App = (props: HelloWorldProps) => (
 <MainRouter />
);