import { observer } from "mobx-react";
import * as React from "react";
import { useParams } from "react-router-dom";
import { IAB, NewAboutStore } from "./NewAboutStore";
import AboutDissplayer from "../AboutPage/Components/AboutDissplayer"
import { ABCONFIG } from "../AboutPage/Consts/AboutConfiigs";

interface Props {
    NewAboutStore: NewAboutStore
}
const NewAboutPage = observer(({ NewAboutStore }: Props) => {
    return (<div>
        {ABCONFIG.map((abc:IAB) => {
            return <button onClick={(_e)=>NewAboutStore.onCahgeAbout(abc)} > {abc.Title}</button>
        })}
        <hr />
        Title:<input value={NewAboutStore.Abaut.Title} type="sting" onChange={(_e) => NewAboutStore.onCahgeTitle((_e.target.value))}></input>
        <button onClick={(_e) => NewAboutStore.getData()}>get File</button>
        <hr />
        Id:<input  value={NewAboutStore.Abaut.Id} type="sting" onChange={(_e) => NewAboutStore.onCahgeId((_e.target.value))}></input>
        <hr />
        Keys:<input type="sting" value={NewAboutStore.Abaut.Keys.join(";")} onChange={(_e) => NewAboutStore.onCahgeKeys((_e.target.value))}></input>
        <hr />
        {NewAboutStore.Abaut.QestionAndAnsvers.map((qa, index) => {
            return (<div key={"QestionAndAnsvers"+index}>
                qestion:<input type="sting" value={qa.Qestion} onChange={(_e) => NewAboutStore.onChangeQestion(index, _e.target.value)}></input>
                ansvers:<input type="sting" value={qa.Ansvers.join(";")} onChange={(_e) => NewAboutStore.onChangeAnsvers(index, _e.target.value)}></input>
                {qa.Ansvers.map(ansV => {
                   return <button id={ansV + "_" + index} onClick={(_e) => NewAboutStore.onChangeGoodAnsver(index, ansV)}>{ansV}</button>
                })}
                goodAnsver:{qa.GoodAnsver}
            </div>)
        })}
        <button onClick={(_e) => NewAboutStore.onAddQestion()}>add QA</button>
        <hr />
        <input type="file" id="input" multiple onChange={(_e) => {
            if (_e.target.files && _e.target.files[0]) {
                var fr = new FileReader();
                fr.onload = function () {
                    NewAboutStore.onAddFile(fr.result);
                }
                fr.readAsDataURL(_e.target.files[0]);
            }
        }}></input>
        {NewAboutStore.Abaut.Files.map((file, index) => {
            return (<div>
                <span>{file.Name}</span>
                <input type="sting" onChange={(_e) => NewAboutStore.onChangeFileTitle(index, _e.target.value)}></input>
            </div>)
        })}
        <hr />
        {NewAboutStore.Abaut.Description.map((ds, c) => {
            return (
                <div>
                    <textarea value={ds} onChange={_e => NewAboutStore.onchangeDEscription(c, (_e.target.value))} ></textarea>
                </div>
            )
        })}
        <button onClick={(_e) => NewAboutStore.onAddDescription()}>add</button>

        <AboutDissplayer Abaut={NewAboutStore.Abaut} />
    </div >)
});

export default NewAboutPage