import { action, computed, makeObservable, observable } from "mobx"



export interface QAA {
    Qestion: string,
    Ansvers: Array<string>,
    GoodAnsver: string,
    Point: number,
}
export interface IAB {
    Title: string,
    Description: string[],
    Files: Array<{ Name: string, Data: string | ArrayBuffer }>,
    Keys: Array<string>,
    Id: string,
    QestionAndAnsvers: Array<QAA>

}
export class NewAboutStore {
    Abaut: IAB = { Title: "", Description: [], Files: [], Keys: [], Id: "", QestionAndAnsvers: [] };

    constructor() {
        makeObservable(this, {
            Abaut: observable,
            onCahgeTitle: action,
            onAddDescription: action,
            onchangeDEscription: action,
            onAddFile: action,
            onCahgeId: action,
            onCahgeKeys: action,
            onChangeQestion: action,
            onChangeAnsvers: action,
            onChangeGoodAnsver: action,
            onCahgeAbout: action,
            onAddQestion:action,
            onChangeFileTitle:action,
        });
    }

    onCahgeTitle(title: string) {
        console.log(title)
        this.Abaut.Title = title;
    }

    onCahgeId(id: string) {
        this.Abaut.Id = id;
    }

    onCahgeAbout(about: IAB) {
        this.Abaut = about;
    }

    onCahgeKeys(value: string) {
        this.Abaut.Keys = value.split(";");
    }

    onAddQestion() {
        this.Abaut.QestionAndAnsvers.push({ Qestion: "", Ansvers: [], Point: 1, GoodAnsver: "" });
    }

    onChangeQestion(id: number, value: string) {
        this.Abaut.QestionAndAnsvers[id].Qestion = value;
    }
    onChangeAnsvers(id: number, value: string) {
        if (!value) this.Abaut.QestionAndAnsvers[id].Ansvers = [];
        this.Abaut.QestionAndAnsvers[id].Ansvers = value.split(";")
    }
    onChangeGoodAnsver(id: number, value: string) {
        this.Abaut.QestionAndAnsvers[id].GoodAnsver = value;
    }

    onAddDescription() {
        this.Abaut.Description.push("");
    }

    onAddFile(data: string | ArrayBuffer) {
        this.Abaut.Files.push({ Name: "", Data: data });
    }

    onChangeFileTitle(index: number, title: string) {
        this.Abaut.Files[index].Name = title;
    }

    getFileByName(fileName: string): string {
        let file = this.Abaut.Files.find(f => f.Name == fileName)
        if (file) return file.Data as string;
        return null;
    }

    getData() {
        var blob = new Blob([JSON.stringify(this.Abaut)], { type: 'text/csv' });
        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, "helper");
        }
        else {
            var elem = window.document.createElement('a');
            elem.href = window.URL.createObjectURL(blob);
            elem.download = "helper";
            document.body.appendChild(elem);
            elem.click();
            document.body.removeChild(elem);
        }
    }

    onchangeDEscription(id: number, value: string) {
        this.Abaut.Description[id] = value;
        console.log(JSON.stringify(this.Abaut));
    }

}