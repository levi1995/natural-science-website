import * as React from "react";
import Qestion from "./Components/Qestion"
import Ansver from "./Components/Ansver"
import { observer } from "mobx-react";
import { Game } from "./GameStore";
import { Link } from "react-router-dom";
import AnimationWarper from "../AnimationWarper/AnimationWarper";


interface Props {
    Game: Game
}

const GamePage = observer(({ Game }: Props) => {
    let res = (<React.Fragment>
        <div style={{
            display: "flex",
            justifyContent: "center",
        }}><Qestion Qestion={Game.CurrentQestonsAndAnsvers.Qestion} /></div>
        <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
            {Game.CurrentQestonsAndAnsvers.Ansvers.map(ansver => {
                return <Ansver Ansver={ansver} onAnsver={(ansver) => { Game.ansver(ansver) }} />
            })}
        </div>


    </React.Fragment>)

    if (!Game.IsGameEnd) {
        if (!Game.CurrentQestonsAndAnsvers) {
            res = (<React.Fragment>
                <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                    <button style={{
                        height: "5rem",
                        width: "10rem",
                        margin: "1rem",
                    }} onClick={(_e) => Game.setCurrentQestonsAndAnsvers()}>Start</button>
                </div>
            </React.Fragment>)
        }
    }
    else {
        res = (<React.Fragment>
            <div className="Ansver" style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                <button style={{
                    height: "5rem",
                    width: "10rem",
                    margin: "1rem",
                }} onClick={(_e) => Game.newGame()}>New Game</button>
            </div>

            <div style={{
                width: "100%",
                height: "5rem",
                textAlign: "center"
            }}><div>Játék vége ({Game.getScore} pont)</div></div>

            <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                <Link to={`/about/${Game.getRelatedKeys.join("-")}`}>Kapcsolódó olvasmányok</Link>
            </div>
           
        </React.Fragment>)
    }
    return (<AnimationWarper><div className="GameMain">{res}</div></AnimationWarper>)
}


);

export default GamePage