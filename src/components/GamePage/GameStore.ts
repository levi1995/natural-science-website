import { action, computed, makeObservable, observable } from "mobx"
import { IAB } from "../NewAbout/NewAboutStore";

export interface IqestionAndAnsver {
    Qestion: string;
    Ansvers: Array<string>;
    GoodAnsver: string;
    Ansver: string;
    RelatedKeys: Array<string>;
}

export class Game {
    QestonsAndAnsvers: Array<IqestionAndAnsver> = [];
    PreviosQestonsAndAnsvers: Array<IqestionAndAnsver> = [];
    CurrentQestonsAndAnsvers: IqestionAndAnsver = null;
    NextQestonsAndAnsvers: Array<IqestionAndAnsver> = [];
    IsGameEnd: boolean = null;

    constructor(abouts: Array<IAB>) {
        makeObservable(this, {
            QestonsAndAnsvers: observable,
            PreviosQestonsAndAnsvers: observable,
            CurrentQestonsAndAnsvers: observable,
            NextQestonsAndAnsvers: observable,
            IsGameEnd: observable,
            setCurrentQestonsAndAnsvers: action,
            ansver: action,
            getScore: computed,
            getRelatedKeys: computed,
        })
        this.IsGameEnd = false;
        let newQestonsAndAnsvers: Array<IqestionAndAnsver> = [];
        abouts.forEach(ab => {
            ab.QestionAndAnsvers.forEach(qa => {
                newQestonsAndAnsvers.push({ Qestion: qa.Qestion, Ansvers: qa.Ansvers, GoodAnsver: qa.GoodAnsver, Ansver: null, RelatedKeys: ab.Keys })
            })
        })
        this.QestonsAndAnsvers = this.help(newQestonsAndAnsvers);
        this.newGame();
    }

    help(obj: object) {
        if (!obj) return [];
        return JSON.parse(JSON.stringify(obj))
    }

    newGame() {
        this.IsGameEnd = false;
        let qas:Array<IqestionAndAnsver> = this.help(this.QestonsAndAnsvers);
        let newQAS = [];
        
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);
        newQAS.push(qas[+(Math.random()*(qas.length-1)).toFixed(0)]);

        this.NextQestonsAndAnsvers = newQAS;
        this.setCurrentQestonsAndAnsvers();
        this.PreviosQestonsAndAnsvers = [];
    }

    setCurrentQestonsAndAnsvers() {
        if (this.NextQestonsAndAnsvers.length > 0) {
            this.CurrentQestonsAndAnsvers = this.NextQestonsAndAnsvers.pop();
        }
    }

    ansver(ansver: string) {
        this.PreviosQestonsAndAnsvers.push({ ...this.CurrentQestonsAndAnsvers, Ansver: ansver });
        if (this.NextQestonsAndAnsvers.length == 0) {
            this.IsGameEnd = true;
        }
        else {
            this.setCurrentQestonsAndAnsvers();
        }
    }

    get getScore() {
        return this.PreviosQestonsAndAnsvers.filter(qa => qa.Ansver == qa.GoodAnsver).length;
    }
    get getRelatedKeys(): Array<string> {
        let ret: Array<string> = []
        this.PreviosQestonsAndAnsvers.forEach(pqaa => {
            ret.push(...pqaa.RelatedKeys)
        });
        return ret;
    }

}