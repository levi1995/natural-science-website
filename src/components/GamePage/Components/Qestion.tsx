import * as React from "react";
interface Props {
  Qestion: string;
}
const Qestion = (props: Props) => (
  <button disabled={true} style={{
    paddingTop: "1rem",
    textAlign: "center",
    minWidth: "10rem",
    minHeight: "5rem",
    marginTop: "1rem",
    border: "0px",
    backgroundColor: "transparent",
    color: "black",
    maxWidth: "90%",
}}> { props.Qestion }</button >
);

export default Qestion