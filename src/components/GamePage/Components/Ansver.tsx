import * as React from "react";
interface Props {
  Ansver: string;
  onAnsver: (ansver: string) => void
}
const Ansver = (props: Props) => (
  <div className={"Ansver"}><button onClick={(_e) => props.onAnsver(props.Ansver)}>{props.Ansver}</button></div>
);

export default Ansver